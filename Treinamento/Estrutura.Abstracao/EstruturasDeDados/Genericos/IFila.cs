﻿namespace Estrutura.Abstracao.EstruturasDeDados.Genericos
{
    public interface IFila<TipoDeDado>
    {
        int Total { get; }
        void Adicionar(TipoDeDado valor);
        bool IsVazia { get; }
        TipoDeDado Pegar();
    }
}