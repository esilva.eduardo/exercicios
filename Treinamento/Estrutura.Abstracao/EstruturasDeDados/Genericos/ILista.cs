﻿using System;

namespace Estrutura.Abstracao.EstruturasDeDados.Genericos
{
    public interface ILista<TipoDeDado>
    {
        int Total { get; }
        void Adicionar(TipoDeDado valor);
        bool IsVazia { get; }
        TipoDeDado Pegar(int index);
        TipoDeDado Remover(int index);
        void ReverterOrdem();
    }
}
