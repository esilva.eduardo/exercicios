﻿namespace Estrutura.Abstracao.EstruturasDeDados.Genericos
{
    public interface IPilha<TipoDeDado>
    {
        int Total { get; }
        void Adicionar(TipoDeDado valor);
        bool IsVazia { get; }
        TipoDeDado Pegar();
    }
}