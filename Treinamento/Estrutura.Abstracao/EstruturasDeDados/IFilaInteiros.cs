﻿namespace Estrutura.Abstracao.EstruturasDeDados
{
    public interface IFilaInteiros
    {
        int Total { get; }
        void Adicionar(int valor);
        bool IsVazia { get; }
        int Pegar();
    }
}