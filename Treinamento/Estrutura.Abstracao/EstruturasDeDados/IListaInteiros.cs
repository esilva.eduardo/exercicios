﻿using System;

namespace Estrutura.Abstracao.EstruturasDeDados
{
    public interface IListaInteiros
    {
        int Total { get; }
        void Adicionar(int valor);
        bool IsVazia { get; }
        int Pegar(int index);
        int Remover(int index);
    }
}
