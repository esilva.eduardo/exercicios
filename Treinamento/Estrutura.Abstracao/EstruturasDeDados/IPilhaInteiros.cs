﻿namespace Estrutura.Abstracao.EstruturasDeDados
{
    public interface IPilhaInteiros
    {
        int Total { get; }
        void Adicionar(int valor);
        bool IsVazia { get; }
        int Pegar();
    }
}