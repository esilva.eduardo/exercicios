﻿namespace Estrutura.EstruturasDeDados
{
    internal class ElementoListaInteirosLincada 
    {
        public int Valor;
        public ElementoListaInteirosLincada Proximo;
        public ElementoListaInteirosLincada() { }
        public void AtacharNoProximo(ElementoListaInteirosLincada elemento)
        {
            if (Proximo == null)
                Proximo = elemento;
            else
                Proximo.AtacharNoProximo(elemento);
        }
        public ElementoListaInteirosLincada PegarProximo(int iteracao)
        {
            if (iteracao == 0)
                return this;
            else
                return Proximo.PegarProximo(iteracao - 1);
        }
    }
}