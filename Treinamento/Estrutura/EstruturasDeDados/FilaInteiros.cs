﻿using Estrutura.Abstracao.EstruturasDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura.EstruturasDeDados
{
    /* 
     * Uma Fila possui as seguintes características; 
     * Ela possui um Início E Fim 
     * Ao Adicionar o primeiro elemento Ele ocupara tanto o Início quanto o Fim da fila. 
     * Ao adicionar um próximo elemento Ele ocupara a posição do final da fila tendendo como próximo elemento o elemento que anteriormente estava no final da fila. 
     * Ao Pegar um elemento da fila, o elemento que estava no início da fila é removido da fila, sendo que o elemento anterior a ele se torna o início da fila. 
     * Uma fila esta vazia sempre que não possui um elemento no início ou no fim da fila;
     */
    public class FilaInteiros : IFilaInteiros
    {
        public int Total => throw new NotImplementedException();

        public bool IsVazia => throw new NotImplementedException();

        // adiciona do final da fila 
        public void Adicionar(int valor)
        {
            throw new NotImplementedException();
        }

        // retira do  inicio da fila
        public int Pegar()
        {
            throw new NotImplementedException();
        }
    }
}
