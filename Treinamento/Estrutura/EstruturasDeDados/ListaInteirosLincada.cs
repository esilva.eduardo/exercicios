﻿using Estrutura.Abstracao.EstruturasDeDados;

namespace Estrutura.EstruturasDeDados
{
    public class ListaInteirosLincada : IListaInteiros
    {
        public int Total { get; private set; }
        private ElementoListaInteirosLincada primeiro;
        public ListaInteirosLincada() { }
        public void Adicionar(int valor)
        {
            Total++;
            if (IsVazia)
            {
                ElementoListaInteirosLincada elemento = new ElementoListaInteirosLincada();
                elemento.Valor = valor;
                primeiro = elemento;
            }
            else
            {
                ElementoListaInteirosLincada elemento = new ElementoListaInteirosLincada();
                elemento.Valor = valor;
                primeiro.AtacharNoProximo(elemento);
            }
        }
        public bool IsVazia
            => primeiro == null;
        public int Pegar(int index)
            => primeiro.PegarProximo(index).Valor;

        public int Remover(int index)
        {
            throw new System.NotImplementedException();
        }
    }
}
