﻿using Estrutura.Abstracao.EstruturasDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura.EstruturasDeDados
{
    /*
     * Uma pilha possui as seguintes características;
     * Ela possui um Topo com o Último Elemento adicionado.
     * Ao Adicionar um elemento na pilha O elemento que está no topo se torna o Anterior Do Elemento Adicionado. E o elemento adicionado se torna o novo topo.
     * Ao Pegar um elemento da pinha, O elemento do topo é retirado da pilha, e o elemento anterior a ele se torna o novo Topo.
     * Uma pilha esta vazia sempre que não possui um topo;
     */
    public class PilhaInteiros : IPilhaInteiros
    {
        public int Total => throw new NotImplementedException();

        public bool IsVazia => throw new NotImplementedException();

        // Adiciona no inicio da pilha
        public void Adicionar(int valor)
        {
            throw new NotImplementedException();
        }

        // Remove do inicio da pilha
        public int Pegar()
        {
            throw new NotImplementedException();
        }
    }
}
