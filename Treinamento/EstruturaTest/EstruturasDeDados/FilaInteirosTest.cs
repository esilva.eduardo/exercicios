﻿using Estrutura.Abstracao.EstruturasDeDados;
using Estrutura.EstruturasDeDados;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstruturaTest.EstruturasDeDados
{
    public class FilaInteirosTest
    {
        private int valorUm;
        private int valorDois;
        private int valorTres;

        [SetUp]
        public void Setup()
        {
            valorUm = 10;
            valorDois = 15;
            valorTres = 16;
        }

        [Test]
        public void Adicionar_UmValor_NaoJogarErro()
        {
            //Arrange
            IFilaInteiros listaInteiros = new FilaInteiros();
            void acao() => listaInteiros.Adicionar(valorUm);

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }

        [Test]
        public void Pegar_UmValor_NaoJogarErro()
        {
            //Arrange
            IFilaInteiros listaInteiros = new FilaInteiros();
            listaInteiros.Adicionar(valorUm);
            void acao() => listaInteiros.Pegar();

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }


        [Test]
        public void Pegar_NenhumValorValor_JogarIndexOutOfRangeException()
        {
            //Arrange
            IFilaInteiros listaInteiros = new FilaInteiros();
            void acao() => listaInteiros.Pegar();

            //Act & Assert
            Assert.Throws<IndexOutOfRangeException>(acao);
        }

        [Test]
        public void Pegar_DoisValores_PrimeiroAdicionadoPrimeiroASerPego()
        {
            //Arrange
            IFilaInteiros listaInteiros = new FilaInteiros();
            listaInteiros.Adicionar(valorUm);
            listaInteiros.Adicionar(valorDois);

            //Act 

            int valorPegoUm = listaInteiros.Pegar();
            int valorPegoDois = listaInteiros.Pegar();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(valorUm, valorPegoUm);
                Assert.AreEqual(valorDois, valorPegoDois);
            });
        }

        [Test]
        public void IsVazia_AdicionarPegarDoisValores_ListaDeveEstarVasia()
        {
            //Arrange
            IFilaInteiros listaInteiros = new FilaInteiros();

            //Act 
            listaInteiros.Adicionar(valorUm);
            listaInteiros.Adicionar(valorDois);
            listaInteiros.Pegar();
            listaInteiros.Pegar();

            // Assert
            Assert.True(listaInteiros.IsVazia);
        }

        [Test]
        public void Total_AdicionarDoisValores_TotalDeveSerDois()
        {
            //Arrange
            IFilaInteiros listaInteiros = new FilaInteiros();

            //Act 
            listaInteiros.Adicionar(valorUm);
            listaInteiros.Adicionar(valorDois);

            // Assert
            Assert.AreEqual(listaInteiros.Total, 2);
        }
    }
}
