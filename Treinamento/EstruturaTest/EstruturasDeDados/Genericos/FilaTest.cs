﻿using Estrutura.Abstracao.EstruturasDeDados;
using Estrutura.Abstracao.EstruturasDeDados.Genericos;
using Estrutura.EstruturasDeDados;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstruturaTest.EstruturasDeDados.Genericos
{
    public class FilaTest
    {
        private int valorUm;
        private int valorDois;
        private string valorStringUm;
        private string valorStringDois;

        [SetUp]
        public void Setup()
        {
            valorUm = 10;
            valorDois = 15;
            valorStringUm = "João";
            valorStringDois = "Maria";
        }

        [Test]
        public void Adicionar_UmValor_NaoJogarErro()
        {
            //Arrange
            IFila<object> fila = FilaTest.ConstruirFilaGenerica<object>();
            void acao() => fila.Adicionar(valorUm);

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }

        [Test]
        public void Pegar_UmValor_NaoJogarErro()
        {
            //Arrange
            IFila<object> fila = FilaTest.ConstruirFilaGenerica<object>();
            fila.Adicionar(valorUm);
            void acao() => fila.Pegar();

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }


        [Test]
        public void Pegar_NenhumValorValor_JogarIndexOutOfRangeException()
        {
            //Arrange
            IFila<object> fila = FilaTest.ConstruirFilaGenerica<object>();
            void acao() => fila.Pegar();

            //Act & Assert
            Assert.Throws<IndexOutOfRangeException>(acao);
        }

        [Test]
        public void Pegar_DoisValores_PrimeiroAdicionadoPrimeiroASerPego()
        {
            //Arrange
            IFila<int> fila = FilaTest.ConstruirFilaGenerica<int>();
            fila.Adicionar(valorUm);
            fila.Adicionar(valorDois);

            //Act 

            int valorPegoUm = fila.Pegar();
            int valorPegoDois = fila.Pegar();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(valorUm, valorPegoUm);
                Assert.AreEqual(valorDois, valorPegoDois);
            });
        }

        [Test]
        public void IsVazia_AdicionarPegarDoisValores_ListaDeveEstarVasia()
        {
            //Arrange
            IFila<string> fila = FilaTest.ConstruirFilaGenerica<string>();

            //Act 
            fila.Adicionar(valorStringUm);
            fila.Adicionar(valorStringDois);
            fila.Pegar();
            fila.Pegar();

            // Assert
            Assert.True(fila.IsVazia);
        }

        [Test]
        public void Total_AdicionarDoisValores_TotalDeveSerDois()
        {
            //Arrange
            IFila<object> fila = FilaTest.ConstruirFilaGenerica<object>();

            //Act 
            fila.Adicionar(valorUm);
            fila.Adicionar(valorDois);

            // Assert
            Assert.AreEqual(fila.Total, 2);
        }

        private static IFila<T> ConstruirFilaGenerica<T>() 
            => throw new NotImplementedException();

    }
}
