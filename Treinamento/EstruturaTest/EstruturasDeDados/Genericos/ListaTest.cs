﻿using Estrutura.Abstracao.EstruturasDeDados;
using Estrutura.Abstracao.EstruturasDeDados.Genericos;
using NUnit.Framework;
using System;

namespace EstruturaTest.EstruturasDeDados.Genericos
{
    public class ListaTest
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Adicionar_UmValor_MesmoValorNaPosicaoZero()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            int valor = 15;
            int retorno;

            //Act
            lista.Adicionar(valor);
            retorno = lista.Pegar(0);

            //Assert
            Assert.AreEqual(valor, retorno);
        }

        [Test]
        public void Adicionar_MultiplosValores_ValoresNaMesmaPosicao()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            int[] valores = new int[] { 1, 55, 77, 44 };

            //Act
            foreach (var valor in valores)
                lista.Adicionar(valor);

            //Assert
            Assert.Multiple(() =>
            {
                for (int i = 0; i < valores.Length; i++)
                    Assert.AreEqual(valores[i], lista.Pegar(i));
            });
        }

        [Test]
        public void IsVazia_NenhumValorAdicionado_ListaEstarVasia()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            //Act & Assert
            Assert.IsTrue(lista.IsVazia);
        }

        [Test]
        public void IsVazia_ValorAdicionado_ListaNaoEstarVasia()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            lista.Adicionar(1);

            //Act & Assert
            Assert.IsFalse(lista.IsVazia);
        }

        [Test]
        public void Total_UmValorAdicionado_TotalSerUm()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            lista.Adicionar(1);
            int total;

            //Act
            total = lista.Total;

            //Assert
            Assert.AreEqual(total, 1);
        }

        [Test]
        public void Remover_AdicionadoTresValoresREmovidoSEgundo_ValorRemovidoDeveSerElemento()
        {
            //Arrange
            int elemento = 2;
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            lista.Adicionar(1);
            lista.Adicionar(elemento);
            lista.Adicionar(3);
            int valorRemovido;

            //Act
            valorRemovido = lista.Remover(1);

            //Assert
            Assert.AreEqual(valorRemovido, elemento);
        }

        [Test]
        public void Remover_AdicionadoTresValoresRemovidoSegundo_TotalDeveDiminuir()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            lista.Adicionar(1);
            lista.Adicionar(2);
            lista.Adicionar(3);
            int totalElementos = lista.Total;

            //Act
            lista.Remover(1);

            //Assert
            Assert.IsTrue(totalElementos > lista.Total);
        }


        [Test]
        public void Remover_RemoverUmElementoSoTendoUmElemento_TotalDeveDiminuir()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            lista.Adicionar(1);

            //Act
            lista.Remover(0);

            //Assert
            Assert.IsTrue(lista.IsVazia);
        }


        [Test]
        public void Remover_AdicionadoTresValoresrRemoverTodos_TotalDeveDiminuir()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();
            lista.Adicionar(1);
            lista.Adicionar(2);
            lista.Adicionar(3);

            //Act
            lista.Remover(0);
            lista.Remover(0);
            lista.Remover(0);

            //Assert
            Assert.IsTrue(lista.IsVazia);
        }

        [Test]
        public void Pegar_EndoidarLista_RetornarOsValoresEmOrdem()
        {
            //Arrange
            ILista<int> lista = ListaTest.ConstruirListaGenerica<int>();

            int valorUm = 12;
            int valorDois = 55;
            int valorTres = 77;

            //Act
            lista.Adicionar(1);
            lista.Adicionar(valorUm);
            lista.Remover(0);
            lista.Adicionar(2);
            lista.Remover(1);
            lista.Adicionar(3);
            lista.Remover(2);
            lista.Adicionar(valorDois);
            lista.Adicionar(4);
            lista.Adicionar(5);
            lista.Adicionar(6);
            lista.Adicionar(7);
            lista.Adicionar(valorTres);

            //Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(valorUm, lista.Pegar(0));
                Assert.AreEqual(valorDois, lista.Pegar(1));
                Assert.AreEqual(valorTres, lista.Pegar(6));
            });
        }

        [Test]
        public void ReverterOrdem_TResValoresDasLista_DevemFicarNaOrdeInversa()
        {
            //Arrange
            string primeiro = nameof(primeiro);
            string segundo = nameof(segundo);
            string terceiro = nameof(terceiro);
            ILista<string> lista = ListaTest.ConstruirListaGenerica<string>();
            lista.Adicionar(primeiro);
            lista.Adicionar(segundo);
            lista.Adicionar(terceiro);

            //Act
            lista.ReverterOrdem();

            //Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(primeiro, lista.Pegar(2));
                Assert.AreEqual(segundo, lista.Pegar(1));
                Assert.AreEqual(terceiro, lista.Pegar(0));
            });
        }

        private static ILista<T> ConstruirListaGenerica<T>()
            => throw new NotImplementedException();
    }
}