﻿using Estrutura.Abstracao.EstruturasDeDados;
using Estrutura.Abstracao.EstruturasDeDados.Genericos;
using Estrutura.EstruturasDeDados;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstruturaTest.EstruturasDeDados.Genericos
{
    public class PilhaTest
    {
        private int valorUm;
        private int valorDois;
        private string valorStringUm;
        private string valorStringDois;

        [SetUp]
        public void Setup()
        {
            valorUm = 10;
            valorDois = 15;
            valorStringUm = "João";
            valorStringDois = "Maria";
        }

        [Test]
        public void Adicionar_UmValor_NaoJogarErro()
        {
            //Arrange
            IPilha<object> pilha = PilhaTest.ConstruirPilhaGenerica<object>();
            void acao() => pilha.Adicionar(valorUm);

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }

        [Test]
        public void Pegar_UmValor_NaoJogarErro()
        {
            //Arrange
            IPilha<object> pilha = PilhaTest.ConstruirPilhaGenerica<object>();
            pilha.Adicionar(valorUm);
            void acao() => pilha.Pegar();

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }


        [Test]
        public void Pegar_NenhumValorValor_JogarIndexOutOfRangeException()
        {
            //Arrange
            IPilha<object> pilha = PilhaTest.ConstruirPilhaGenerica<object>();
            void acao() => pilha.Pegar();

            //Act & Assert
            Assert.Throws<IndexOutOfRangeException>(acao);
        }

        [Test]
        public void Pegar_DoisValores_UltimoAdicionadoPrimeiroASerPego()
        {
            //Arrange
            IPilha<string> pilha = PilhaTest.ConstruirPilhaGenerica<string>();
            pilha.Adicionar(valorStringUm);
            pilha.Adicionar(valorStringDois);

            //Act
            string valorPegoDois = pilha.Pegar();
            string valorPegoUm = pilha.Pegar();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(valorStringUm, valorPegoUm);
                Assert.AreEqual(valorStringDois, valorPegoDois);
            });
        }

        [Test]
        public void IsVazia_AdicionarPegarDoisValores_ListaDeveEstarVasia()
        {
            //Arrange
            IPilha<int> pilha = PilhaTest.ConstruirPilhaGenerica<int>();

            //Act 
            pilha.Adicionar(valorUm);
            pilha.Adicionar(valorDois);
            pilha.Pegar();
            pilha.Pegar();

            // Assert
            Assert.True(pilha.IsVazia);
        }

        [Test]
        public void Total_AdicionarDoisValores_TotalDeveSerDois()
        {
            //Arrange
            IPilha<object> pilha = PilhaTest.ConstruirPilhaGenerica<object>();

            //Act 
            pilha.Adicionar(valorUm);
            pilha.Adicionar(valorDois);

            // Assert
            Assert.AreEqual(pilha.Total, 2);
        }

        private static IPilha<T> ConstruirPilhaGenerica<T>()
            => throw new NotImplementedException();
    }
}
