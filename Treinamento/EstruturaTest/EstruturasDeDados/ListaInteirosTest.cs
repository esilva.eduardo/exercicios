﻿using Estrutura.Abstracao.EstruturasDeDados;
using Estrutura.EstruturasDeDados;
using NUnit.Framework;

namespace EstruturaTest.EstruturasDeDados
{
    public class ListaInteirosTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Adicionar_UmValor_MesmoValorNaPosicaoZero()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            int valor = 15;
            int retorno;

            //Act
            listaInteiros.Adicionar(valor);
            retorno = listaInteiros.Pegar(0);

            //Assert
            Assert.AreEqual(valor, retorno);
        }

        [Test]
        public void Adicionar_MultiplosValores_ValoresNaMesmaPosicao()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            int[] valores = new int[] { 1, 55, 77, 44 };

            //Act
            foreach (var valor in valores)
                listaInteiros.Adicionar(valor);

            //Assert
            Assert.Multiple(() =>
            {
                for (int i = 0; i < valores.Length; i++)
                    Assert.AreEqual(valores[i], listaInteiros.Pegar(i));
            });
        }


        [Test]
        public void IsVazia_NenhumValorAdicionado_ListaEstarVasia()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            //Act & Assert
            Assert.IsTrue(listaInteiros.IsVazia);
        }

        [Test]
        public void IsVazia_ValorAdicionado_ListaNaoEstarVasia()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            listaInteiros.Adicionar(1);

            //Act & Assert
            Assert.IsFalse(listaInteiros.IsVazia);
        }

        [Test]
        public void Total_UmValorAdicionado_TotalSerUm()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            listaInteiros.Adicionar(1);
            int total;

            //Act
            total = listaInteiros.Total;

            //Assert
            Assert.AreEqual(total, 1);
        }

        [Test]
        public void Remover_AdicionadoTresValoresREmovidoSEgundo_ValorRemovidoDeveSerElemento()
        {
            //Arrange
            int elemento = 2;
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            listaInteiros.Adicionar(1);
            listaInteiros.Adicionar(elemento);
            listaInteiros.Adicionar(3);
            int valorRemovido;

            //Act
            valorRemovido = listaInteiros.Remover(1);

            //Assert
            Assert.AreEqual(valorRemovido, elemento);
        }

        [Test]
        public void Remover_AdicionadoTresValoresRemovidoSegundo_TotalDeveDiminuir()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            listaInteiros.Adicionar(1);
            listaInteiros.Adicionar(2);
            listaInteiros.Adicionar(3);
            int totalElementos = listaInteiros.Total;

            //Act
            listaInteiros.Remover(1);

            //Assert
            Assert.IsTrue(totalElementos > listaInteiros.Total);
        }


        [Test]
        public void Remover_RemoverUmElementoSoTendoUmElemento_TotalDeveDiminuir()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            listaInteiros.Adicionar(1);

            //Act
            listaInteiros.Remover(0);

            //Assert
            Assert.IsTrue(listaInteiros.IsVazia);
        }


        [Test]
        public void Remover_AdicionadoTresValoresrRemoverTodos_TotalDeveDiminuir()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();
            listaInteiros.Adicionar(1);
            listaInteiros.Adicionar(2);
            listaInteiros.Adicionar(3);

            //Act
            listaInteiros.Remover(0);
            listaInteiros.Remover(0);
            listaInteiros.Remover(0);

            //Assert
            Assert.IsTrue(listaInteiros.IsVazia);
        }

        [Test]
        public void Pegar_EndoidarLista_RetornarOsValoresEmOrdem()
        {
            //Arrange
            IListaInteiros listaInteiros = new ListaInteirosLincada();

            int valorUm = 12;
            int valorDois = 55;
            int valorTres = 77;

            //Act
            listaInteiros.Adicionar(1);
            listaInteiros.Adicionar(valorUm);
            listaInteiros.Remover(0);
            listaInteiros.Adicionar(2);
            listaInteiros.Remover(1);
            listaInteiros.Adicionar(3);
            listaInteiros.Remover(2);
            listaInteiros.Adicionar(valorDois);
            listaInteiros.Adicionar(4);
            listaInteiros.Adicionar(5);
            listaInteiros.Adicionar(6);
            listaInteiros.Adicionar(7);
            listaInteiros.Adicionar(valorTres);

            //Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(valorUm, listaInteiros.Pegar(0));
                Assert.AreEqual(valorDois, listaInteiros.Pegar(1));
                Assert.AreEqual(valorTres, listaInteiros.Pegar(6));
            });
        }
    }
}