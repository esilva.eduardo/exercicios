﻿using Estrutura.Abstracao.EstruturasDeDados;
using Estrutura.EstruturasDeDados;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstruturaTest.EstruturasDeDados
{
    public class PilhaInteirosTest
    {
        private int valorUm;
        private int valorDois;
        private int valorTres;

        [SetUp]
        public void Setup()
        {
            valorUm = 10;
            valorDois = 15;
            valorTres = 16;
        }

        [Test]
        public void Adicionar_UmValor_NaoJogarErro()
        {
            //Arrange
            IPilhaInteiros pilhaInteiros = new PilhaInteiros();
            void acao() => pilhaInteiros.Adicionar(valorUm);

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }

        [Test]
        public void Pegar_UmValor_NaoJogarErro()
        {
            //Arrange
            IPilhaInteiros pilhaInteiros = new PilhaInteiros();
            pilhaInteiros.Adicionar(valorUm);
            void acao() => pilhaInteiros.Pegar();

            //Act & Assert
            Assert.DoesNotThrow(acao);
        }


        [Test]
        public void Pegar_NenhumValorValor_JogarIndexOutOfRangeException()
        {
            //Arrange
            IPilhaInteiros pilhaInteiros = new PilhaInteiros();
            void acao() => pilhaInteiros.Pegar();

            //Act & Assert
            Assert.Throws<IndexOutOfRangeException>(acao);
        }

        [Test]
        public void Pegar_DoisValores_UltimoAdicionadoPrimeiroASerPego()
        {
            //Arrange
            IPilhaInteiros pilhaInteiros = new PilhaInteiros();
            pilhaInteiros.Adicionar(valorUm);
            pilhaInteiros.Adicionar(valorDois);

            //Act
            int valorPegoDois = pilhaInteiros.Pegar();
            int valorPegoUm = pilhaInteiros.Pegar();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(valorUm, valorPegoUm);
                Assert.AreEqual(valorDois, valorPegoDois);
            });
        }

        [Test]
        public void IsVazia_AdicionarPegarDoisValores_ListaDeveEstarVasia()
        {
            //Arrange
            IPilhaInteiros pilhaInteiros = new PilhaInteiros();

            //Act 
            pilhaInteiros.Adicionar(valorUm);
            pilhaInteiros.Adicionar(valorDois);
            pilhaInteiros.Pegar();
            pilhaInteiros.Pegar();

            // Assert
            Assert.True(pilhaInteiros.IsVazia);
        }

        [Test]
        public void Total_AdicionarDoisValores_TotalDeveSerDois()
        {
            //Arrange
            IPilhaInteiros pilhaInteiros = new PilhaInteiros();

            //Act 
            pilhaInteiros.Adicionar(valorUm);
            pilhaInteiros.Adicionar(valorDois);

            // Assert
            Assert.AreEqual(pilhaInteiros.Total, 2);
        }
    }
}
