﻿using System;

namespace ModelosDedados.Modelos
{
    //public / private / internal

    public class Usuario
    {
        private int id;

        public Usuario()
        {
        }


        public void BotarId(int id)
        {
            this.id = id;
        }

        public int PegarId()
        {
            return this.id;
        }

        public int TrocarId(int id) // 15
        {
            int idVelho; // Posição de meoria

            idVelho = this.id; // salvo o valor do id velho em uma variavel

            this.id = id; // boto o valor

            return idVelho;
        }


    }
}
